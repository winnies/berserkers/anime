#ifndef CAMERA_H
#define CAMERA_H

#include <QVector3D>
#include <QMatrix4x4>

class Camera
{
public:
    Camera(const QVector3D& position);
    bool updatePos(int key);
    void updateDir(int x, int y);
    QMatrix4x4 getViewMatrix() const;

private:
    QVector3D m_position;
    QVector3D m_direction;
    QVector3D m_right;
    float m_speed = 0.5f;
    float m_yaw = 0.f, m_pitch = 0.f;
};

#endif // CAMERA_H
