// Basé sur :
// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"

#include <QDebug>
#include <QSurfaceFormat>
#include <QOpenGLDebugLogger>
#include <QMatrix4x4>
#include <QPainter>
#include <QPainterPath>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

//#define GL_DEBUG

struct MyTraits : public OpenMesh::DefaultTraits
{
    VertexAttributes(OpenMesh::Attributes::TexCoord2D | OpenMesh::Attributes::Normal);
};

typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent),
    camera(QVector3D(-20, 4, 0))
{
    QSurfaceFormat sf;
    sf.setMajorVersion(2);
    sf.setMinorVersion(0);
    sf.setProfile(QSurfaceFormat::NoProfile);
//    QSurfaceFormat fmt;
//    fmt.setVersion( 3, 3 );
//    fmt.setProfile( QSurfaceFormat::CoreProfile );
//    QSurfaceFormat::setDefaultFormat( fmt );
//    sf.setDepthBufferSize(24);
//    sf.setSamples(16);
#ifdef GL_DEBUG
    sf.setOption(QSurfaceFormat::DebugContext);
#endif
    setFormat(sf);

    setEnabled(true);                   // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus);    // accepte focus
    setFocus();                         // donne le focus

    timer = new QTimer(this);
    timer->setInterval(15);           // msec
    connect (timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer->start();
    elapsedTimer.start();
}


GLArea::~GLArea()
{
    makeCurrent();
    tearGLObjects();
    doneCurrent();
}

void GLArea::updateAnim()
{
    makeCurrent();

    //------------------------------- Création des trajectoires -----------------------------//

    GLfloat color_path[] = {
        1.0f, 1.0f, 0.f
    };

    QVector<GLfloat> vertData_path;
    for(size_t i = 0; i < droneAnimation->dronesCount(); i++)
    {
        for(size_t j = 0; j < droneAnimation->framesCount(); j++)
        {
            // coordonnées sommets
            for(size_t k = 0; k < 3; k++)
                vertData_path.append(droneAnimation->drones[i][j].position[k] * 0.01);
            // couleur
            for(size_t k = 0; k < 3; k++)
                vertData_path.append(color_path[k]);
        }
    }

    vbo_path.create();
    vbo_path.bind();
    vbo_path.allocate(vertData_path.constData(), vertData_path.count() * int(sizeof(GLfloat)));

    vbo_vert_lines.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    vbo_vert_lines.create();

    //------------------------------- Création des billboards (nom des drones) -----------------------------//

    size_t dronesCount = droneAnimation->dronesCount();
    textures_names.reserve(dronesCount);
    textures_names_size.reserve(dronesCount);
    for(size_t i = 0; i < dronesCount; ++i) {
        QImage img = renderText(QString("DRONE %1").arg(i));
        QOpenGLTexture * texture = new QOpenGLTexture(img);
        texture->generateMipMaps();
        texture->setWrapMode(QOpenGLTexture::ClampToEdge);
        texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
        texture->setMagnificationFilter(QOpenGLTexture::Linear);
        textures_names.push_back(texture);
        textures_names_size.push_back(QVector2D(img.size().width(), img.size().height()) / std::max(img.size().width(), img.size().height()));
    }

    float taille = 1.f;

    GLfloat vertices[] = {
       -taille, taille, 0.0f,
       -taille, -taille, 0.0f,
        taille, taille, 0.0f,
       -taille,-taille, 0.0f,
        taille,-taille, 0.0f,
        taille, taille, 0.0f
    };

    GLfloat texCoords[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
    };

    QVector<GLfloat> vertData_billboard;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_billboard.append(vertices[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_billboard.append(texCoords[i*2+j]);
    }

    vbo_billboard.create();
    vbo_billboard.bind();
    vbo_billboard.allocate(vertData_billboard.constData(), vertData_billboard.count() * int(sizeof(GLfloat)));

    doneCurrent();
}


void GLArea::initializeGL()
{
    initializeOpenGLFunctions();

#ifdef GL_DEBUG
    QOpenGLDebugLogger* m_debugLogger = new QOpenGLDebugLogger(context());
    if (m_debugLogger->initialize()) {
      qDebug() << "GL_DEBUG Debug Logger" << m_debugLogger << "\n";
      connect(m_debugLogger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(messageLogged(QOpenGLDebugMessage)));
      m_debugLogger->startLogging();
    }
#endif

    glClearColor(0.5f,0.5f,1.0f,1.0f);
    glEnable(GL_DEPTH_TEST);

    makeGLObjects();

    // shader du sol
    program_sol = new QOpenGLShaderProgram(this);
    program_sol->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vsh");
    program_sol->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.fsh");
    if (! program_sol->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_sol->log();
    }
    program_sol->bind();
    program_sol->setUniformValue("texture", 0);

    // shader des drones
    program_drone = new QOpenGLShaderProgram(this);
    program_drone->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/drone.vsh");
    program_drone->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/drone.fsh");
    if (! program_drone->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_drone->log();
    }
    program_drone->bind();
    program_drone->setUniformValue("texture", 0);

    // shader des différents repères visuels
    program_lines = new QOpenGLShaderProgram(this);
    program_lines->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/axis.vsh");
    program_lines->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/axis.fsh");
    if (! program_lines->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_lines->log();
    }

    // shader de la skybox
    program_skybox = new QOpenGLShaderProgram(this);
    program_skybox->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/skybox.vsh");
    program_skybox->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/skybox.fsh");
    if (! program_skybox->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_skybox->log();
    }
    program_skybox->bind();
    program_skybox->setUniformValue("skybox", 0);

    program_billboard = new QOpenGLShaderProgram(this);
    program_billboard->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/billboard.vsh");
    program_billboard->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/billboard.fsh");
    if (! program_billboard->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_billboard->log();
    }
    program_billboard->bind();
    program_billboard->setUniformValue("texture", 0);
}


void GLArea::makeGLObjects()
{
   //------------------------------- Création du sol -----------------------------//

    GLfloat vertices_sol[] = {
       -worldSize, 0.0f,-worldSize,
       -worldSize, 0.0f, worldSize,
        worldSize, 0.0f, worldSize,
        worldSize, 0.0f, worldSize,
        worldSize, 0.0f,-worldSize,
       -worldSize, 0.0f,-worldSize
    };

    GLfloat texCoords_sol[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f
        };

    QVector<GLfloat> vertData_sol;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_sol.append(vertices_sol[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_sol.append(texCoords_sol[i*2+j]);
    }

    vbo_sol.create();
    vbo_sol.bind();
    vbo_sol.allocate(vertData_sol.constData(), vertData_sol.count() * int(sizeof(GLfloat)));

    // Affectation de la texture au sol
    QImage image_sol(":/textures/ground.png");
    if (image_sol.isNull())
        qDebug() << "load image ground.png failed";
    texture_sol = new QOpenGLTexture(image_sol);
    texture_sol->generateMipMaps();
    texture_sol->setWrapMode(QOpenGLTexture::ClampToEdge);
    texture_sol->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
    texture_sol->setMagnificationFilter(QOpenGLTexture::Linear);

    //------------------------------- Création d'un drone -----------------------------//

    MyMesh mesh;

    OpenMesh::IO::Options opt;
    opt += OpenMesh::IO::Options::VertexTexCoord;
    opt += OpenMesh::IO::Options::VertexNormal;

    QFile f(":/objects/drone.obj");
    f.open(QIODevice::ReadOnly);
    std::istringstream is(f.readAll().toStdString());

    if (!OpenMesh::IO::read_mesh(mesh, is, ".obj", opt))
    {
        qDebug("Error reading drone mesh");
        exit(1);
    }

    QVector<GLfloat> vertData_drone;
    vertData_drone.reserve(mesh.n_vertices() * 8);

    // Parcours du mesh et stockage des attributs
    for (auto v : mesh.vertices()) {
        const auto &point = mesh.point(v);
        const auto &uv = mesh.texcoord2D(v);
        const auto &normal = mesh.normal(v);
        vertData_drone.push_back(point[0]); vertData_drone.push_back(point[1]); vertData_drone.push_back(point[2]);
        vertData_drone.push_back(uv[0]); vertData_drone.push_back(uv[1]);
        vertData_drone.push_back(normal[0]); vertData_drone.push_back(normal[1]); vertData_drone.push_back(normal[2]);
    }

    QVector<GLuint> elemData_drone;
    elemData_drone.reserve(mesh.n_faces() * 3);

    for (auto face : mesh.faces()) {
        for(auto fv_it=mesh.fv_ccwiter(face); fv_it.is_valid(); ++fv_it) {
            elemData_drone.push_back(fv_it->idx());
        }
    }

    drone_vert_nb = mesh.n_faces() * 3;

    vbo_drone_verts.create();
    vbo_drone_verts.bind();
    vbo_drone_verts.allocate(vertData_drone.constData(), vertData_drone.count() * int(sizeof(GLfloat)));

    vbo_drones_elems = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    vbo_drones_elems.create();
    vbo_drones_elems.bind();
    vbo_drones_elems.allocate(elemData_drone.constData(), elemData_drone.count() * int(sizeof(GLuint)));

    // Affectation de la texture au drone
    QImage image_drone(":/textures/drone.png");
    if (image_drone.isNull())
        qDebug() << "load image drone.png failed";
    texture_drone = new QOpenGLTexture(image_drone.mirrored(false, true));
    texture_drone->generateMipMaps();
    texture_drone->setWrapMode(QOpenGLTexture::ClampToEdge);
    texture_drone->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
    texture_drone->setMagnificationFilter(QOpenGLTexture::Linear);

    //------------------------------- Création de l'axe repère -----------------------------//

    float axis_size = 20.f;

    GLfloat vertices_axis[] = {
        axis_size, 0.f, 0.f,
        0, 0.f, 0.f,
        0.f, axis_size, 0.f,
        0.f, 0, 0.f,
        0.f, 0.f, axis_size,
        0.f, 0.f, 0
    };

    GLfloat color_axis[] = {
        1.0f, 0.f, 0.f,
        1.0f, 0.f, 0.f,
        0.f, 1.0f, 0.f,
        0.f, 1.0f, 0.f,
        0.f, 0.f, 1.0f,
        0.f, 0.f, 1.0f
    };

    QVector<GLfloat> vertData_axis;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_axis.append(vertices_axis[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 3; j++)
            vertData_axis.append(color_axis[i*3+j]);
    }

    vbo_axis.create();
    vbo_axis.bind();
    vbo_axis.allocate(vertData_axis.constData(), vertData_axis.count() * int(sizeof(GLfloat)));

    //------------------------------- Création de la grille 2D -----------------------------//

     QVector<GLfloat> vertData_grid;

     for(int i = -worldSize; i <= worldSize; i+= cellSize)
     {
         vertData_grid.append(i);
         vertData_grid.append(0);
         vertData_grid.append(-worldSize);
         vertData_grid.append(0.f);
         vertData_grid.append(1.f);
         vertData_grid.append(1.f);
         vertData_grid.append(i);
         vertData_grid.append(0);
         vertData_grid.append(worldSize);
         vertData_grid.append(0.f);
         vertData_grid.append(1.f);
         vertData_grid.append(1.f);
     }

     for(int i = -worldSize; i <= worldSize; i+= cellSize)
     {
         vertData_grid.append(-worldSize);
         vertData_grid.append(0);
         vertData_grid.append(i);
         vertData_grid.append(0.f);
         vertData_grid.append(1.f);
         vertData_grid.append(1.f);
         vertData_grid.append(worldSize);
         vertData_grid.append(0);
         vertData_grid.append(i);
         vertData_grid.append(0.f);
         vertData_grid.append(1.f);
         vertData_grid.append(1.f);
     }

     vbo_grid.create();
     vbo_grid.bind();
     vbo_grid.allocate(vertData_grid.constData(), vertData_grid.count() * int(sizeof(GLfloat)));

    //------------------------------- Création de la skybox -----------------------------//

    GLfloat skyboxVertices[] = {
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };

    vbo_skybox.create();
    vbo_skybox.bind();
    vbo_skybox.allocate(skyboxVertices, 108 * sizeof(GLfloat));

    const QImage posx = QImage(":/textures/skybox/right.jpg").convertToFormat(QImage::Format_RGBA8888);
    const QImage negx = QImage(":/textures/skybox/left.jpg").convertToFormat(QImage::Format_RGBA8888);
    const QImage posy = QImage(":/textures/skybox/top.jpg").convertToFormat(QImage::Format_RGBA8888);
    const QImage negy = QImage(":/textures/skybox/bottom.jpg").convertToFormat(QImage::Format_RGBA8888);
    const QImage posz = QImage(":/textures/skybox/front.jpg").convertToFormat(QImage::Format_RGBA8888);
    const QImage negz = QImage(":/textures/skybox/back.jpg").convertToFormat(QImage::Format_RGBA8888);

    texture_skybox = new QOpenGLTexture(QOpenGLTexture::TargetCubeMap);
    texture_skybox->create();
//    texture_skybox->setSize(posx.width(), posx.height(), posx.depth());
//    texture_skybox->setFormat(QOpenGLTexture::RGBA8_UNorm);
//    texture_skybox->allocateStorage();
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapPositiveX,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)posx.constBits(), 0);
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapNegativeX,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)negx.constBits(), 0);
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapPositiveY,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)posy.constBits(), 0);
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapNegativeY,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)negy.constBits(), 0);
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapPositiveZ,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)posz.constBits(), 0);
//    texture_skybox->setData(0, 0, QOpenGLTexture::CubeMapNegativeZ,
//                            QOpenGLTexture::RGBA, QOpenGLTexture::UInt8,
//                            (const void*)negz.constBits(), 0);

    texture_skybox->bind();
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, posx.width(), posx.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, posx.constBits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, negx.width(), negx.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, negx.constBits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, posy.width(), posy.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, posy.constBits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, negy.width(), negy.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, negy.constBits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, posz.width(), posz.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, posz.constBits());
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, negz.width(), negz.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, negz.constBits());
    texture_skybox->setWrapMode(QOpenGLTexture::ClampToEdge);
    texture_skybox->setMinificationFilter(QOpenGLTexture::Linear);
    texture_skybox->setMagnificationFilter(QOpenGLTexture::Linear);

    doneCurrent();
    updateAnim();
    makeCurrent();
}


void GLArea::tearGLObjects()
{
    vbo_sol.destroy();
    delete program_sol;
    program_sol = nullptr;
    delete texture_sol;
    texture_sol = nullptr;

    vbo_drone_verts.destroy();
    vbo_drones_elems.destroy();
    delete program_drone;
    program_drone = nullptr;
    delete texture_drone;
    texture_drone = nullptr;

    vbo_axis.destroy();
    vbo_path.destroy();
    vbo_grid.destroy();
    vbo_vert_lines.destroy();
    delete program_lines;
    program_lines = nullptr;

    vbo_skybox.destroy();
    delete  program_skybox;
    program_skybox = nullptr;
    delete  texture_skybox;
    texture_skybox = nullptr;
}


void GLArea::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    windowRatio = float(w) / h;
}


void GLArea::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Matrice de projection
    QMatrix4x4 projectionMatrix;
    projectionMatrix.perspective(45.0f, windowRatio, 0.1f, 1000.0f);

    // Matrice de vue (caméra)
    QMatrix4x4 viewMatrix = camera.getViewMatrix();

    // Matrice du modèle du monde
    QMatrix4x4 modelMatrixWorld;
    modelMatrixWorld.translate(0.0f, 0.0f, 0.0f);

    //------------------------------- Affichage du sol -----------------------------//

    vbo_sol.bind();
    program_sol->bind(); // active le shader program du sol

    program_sol->setUniformValue("projectionMatrix", projectionMatrix);
    program_sol->setUniformValue("viewMatrix", viewMatrix);
    program_sol->setUniformValue("modelMatrix", modelMatrixWorld);

    program_sol->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    program_sol->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    program_sol->enableAttributeArray("in_position");
    program_sol->enableAttributeArray("in_uv");

    texture_sol->bind(0);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    texture_sol->release(0);

    program_sol->disableAttributeArray("in_position");
    program_sol->disableAttributeArray("in_uv");
    program_sol->release();

    //------------------------------- Affichage des drones -----------------------------//

    vbo_drone_verts.bind();
    vbo_drones_elems.bind();
    program_drone->bind(); // active le shader program du drone

    program_drone->setUniformValue("projectionMatrix", projectionMatrix);
    program_drone->setUniformValue("viewMatrix", viewMatrix);

    program_drone->setUniformValue("lightPos", QVector3D(100, 100, 100));
    program_drone->setUniformValue("lightColor",  QVector3D(1, 1, 1));

    program_drone->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 8 * sizeof(GLfloat));
    program_drone->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 8 * sizeof(GLfloat));
    program_drone->setAttributeBuffer("in_normal", GL_FLOAT, 5 * sizeof(GLfloat), 3, 8 * sizeof(GLfloat));

    program_drone->enableAttributeArray("in_position");
    program_drone->enableAttributeArray("in_uv");
    program_drone->enableAttributeArray("in_normal");

    texture_drone->bind(0);
    for(size_t i = 0; i < droneAnimation->dronesCount(); i++)
    {
        auto modelMatrix = droneAnimation->matrix(i);
        program_drone->setUniformValue("modelMatrix", modelMatrix);
        program_drone->setUniformValue("normalMatrix", modelMatrix.normalMatrix());
        glDrawElements(GL_TRIANGLES, drone_vert_nb, GL_UNSIGNED_INT, NULL);
    }
    texture_drone->release(0);

    program_drone->disableAttributeArray("in_position");
    program_drone->disableAttributeArray("in_uv");
    program_drone->disableAttributeArray("in_normal");
    program_drone->release();

    //------------------------------- Affichage des repères visuels -----------------------------//

    // Axes
    if(showAxis) {
        vbo_axis.bind();
        program_lines->bind();

        program_lines->setUniformValue("projectionMatrix", projectionMatrix);
        program_lines->setUniformValue("viewMatrix", viewMatrix);
        program_lines->setUniformValue("modelMatrix", modelMatrixWorld);

        program_lines->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        program_lines->setAttributeBuffer("in_color", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
        program_lines->enableAttributeArray("in_position");
        program_lines->enableAttributeArray("in_color");

        glDrawArrays(GL_LINES, 0, 6);

        program_lines->disableAttributeArray("in_position");
        program_lines->disableAttributeArray("in_color");
        program_lines->release();
    }

    // Trajectoires
    if(showPath) {
        vbo_path.bind();
        program_lines->bind();

        program_lines->setUniformValue("projectionMatrix", projectionMatrix);
        program_lines->setUniformValue("viewMatrix", viewMatrix);
        program_lines->setUniformValue("modelMatrix", modelMatrixWorld);

        program_lines->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        program_lines->setAttributeBuffer("in_color", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
        program_lines->enableAttributeArray("in_position");
        program_lines->enableAttributeArray("in_color");

        for(size_t i = 0; i < droneAnimation->dronesCount(); i++)
            glDrawArrays(GL_LINE_STRIP, i * droneAnimation->framesCount(), droneAnimation->framesCount());

        program_lines->disableAttributeArray("in_position");
        program_lines->disableAttributeArray("in_color");
        program_lines->release();
    }

    // Grille 2D
    if (showGrid) {
        vbo_grid.bind();
        program_lines->bind();

        program_lines->setUniformValue("projectionMatrix", projectionMatrix);
        program_lines->setUniformValue("viewMatrix", viewMatrix);
        program_lines->setUniformValue("modelMatrix", modelMatrixWorld);

        program_lines->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        program_lines->setAttributeBuffer("in_color", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
        program_lines->enableAttributeArray("in_position");
        program_lines->enableAttributeArray("in_color");

        glDrawArrays(GL_LINES, 0, 4 * cellSize);

        program_lines->disableAttributeArray("in_position");
        program_lines->disableAttributeArray("in_color");
        program_lines->release();
    }

    // Nom des drones
    if (showName) {
        vbo_billboard.bind();
        program_billboard->bind();

        program_billboard->setUniformValue("projectionMatrix", projectionMatrix);
        program_billboard->setUniformValue("viewMatrix", viewMatrix);
        program_billboard->setUniformValue("total_time", 0.f);
        program_billboard->setUniformValue("time", 0.f);
        program_billboard->setUniformValue("color", QVector4D(0, 0, 0, 0));

        program_billboard->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
        program_billboard->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
        program_billboard->enableAttributeArray("in_position");
        program_billboard->enableAttributeArray("in_uv");

        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        for(size_t i = 0; i < droneAnimation->dronesCount(); ++i) {
            auto pos = droneAnimation->matrix(i) * QVector3D(0, 0, 0);
            QMatrix4x4 modelMatrix;
            modelMatrix.translate(pos + QVector3D(0, 1, 0));
            program_billboard->setUniformValue("modelMatrix", modelMatrix);
            program_billboard->setUniformValue("particleSize", textures_names_size[i]);

            textures_names[i]->bind(0);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            textures_names[i]->release(0);
        }

        glDisable(GL_BLEND);

        program_billboard->disableAttributeArray("in_position");
        program_billboard->disableAttributeArray("in_uv");
        program_billboard->release();
    }

    // Lignes verticales depuis le centre des drones
    if (showVerticalLines) {
        static QVector<GLfloat> vertData_path;
        vertData_path.reserve(droneAnimation->dronesCount() * 3 * 4);
        vertData_path.clear();
        for(size_t i = 0; i < droneAnimation->dronesCount(); i++)
        {
            auto pos = droneAnimation->matrix(i) * QVector3D(0, 0, 0);
            vertData_path << pos.x() << pos.y() << pos.z();
            vertData_path << 0.f << 0.f << 0.f;
            vertData_path << pos.x() << 0.f << pos.z();
            vertData_path << 0.f << 0.f << 0.f;
        }

        vbo_vert_lines.bind();
        vbo_vert_lines.allocate(vertData_path.constData(), vertData_path.count() * int(sizeof(GLfloat)));

        program_lines->bind();

        program_lines->setUniformValue("projectionMatrix", projectionMatrix);
        program_lines->setUniformValue("viewMatrix", viewMatrix);
        program_lines->setUniformValue("modelMatrix", modelMatrixWorld);

        program_lines->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
        program_lines->setAttributeBuffer("in_color", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
        program_lines->enableAttributeArray("in_position");
        program_lines->enableAttributeArray("in_color");

        glDrawArrays(GL_LINES, 0, droneAnimation->dronesCount() * 2);

        program_lines->disableAttributeArray("in_position");
        program_lines->disableAttributeArray("in_color");
        program_lines->release();
    }

    //------------------------------- Affichage de la skybox -----------------------------//

    glDepthFunc(GL_LEQUAL);
    vbo_skybox.bind();
    program_skybox->bind();
    program_skybox->setUniformValue("projection", projectionMatrix);

    QMatrix4x4 viewWithoutTranslation = viewMatrix;
    viewWithoutTranslation.setColumn(3, {0, 0, 0, 1});
    program_skybox->setUniformValue("view", viewWithoutTranslation);

    program_skybox->setAttributeBuffer("aPos", GL_FLOAT, 0, 3, 3 * sizeof(GLfloat));
    program_skybox->enableAttributeArray("aPos");

    texture_skybox->bind(0);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    texture_skybox->release(0);

    program_skybox->disableAttributeArray("aPos");
    program_skybox->release();
    vbo_skybox.release();
    glDepthFunc(GL_LESS);
}


void GLArea::keyPressEvent(QKeyEvent *ev)
{
    if(camera.updatePos(ev->key())) update();
}


void GLArea::keyReleaseEvent(QKeyEvent *ev)
{

}


void GLArea::mousePressEvent(QMouseEvent *ev)
{
    QCursor cursor(Qt::BlankCursor);
    setCursor(cursor);
    QPoint glob = mapToGlobal(QPoint(width()/2, height()/2));
    QCursor::setPos(glob);
}


void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    unsetCursor();
}


void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    float dx = ev->x() - width()/2;
    float dy = ev->y() - height()/2;

    camera.updateDir(dx, -dy);

    QPoint glob = mapToGlobal(QPoint(width()/2, height()/2));
    QCursor::setPos(glob);

    update();
}

QImage GLArea::renderText(const QString &text)
{
    float margin = 5.f;
    QPainterPath p;
    p.addText(margin, -margin, QFont("Helvetica", 14), text);

    auto boundingRect = p.boundingRect().marginsAdded(QMarginsF() + margin).toAlignedRect();

    QImage img(boundingRect.size(), QImage::Format_RGB32);
    img.fill(Qt::black);

    QPainter painter(&img);
//    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.scale(1.f, -1.f);
    painter.fillPath(p, Qt::white);
    painter.end();

//    QImage img(500, 500, QImage::Format_RGB32);
//    img.fill(Qt::black);

//    QPainter painter(&img);
//    painter.setPen(Qt::white);
//    painter.drawText(20, 50, 100, 100, Qt::AlignHCenter & Qt::AlignVCenter, text);
//    painter.end();

    return img.mirrored(false, true);
}


void GLArea::onTimeout()
{
    static qint64 old_chrono = elapsedTimer.elapsed(); // static : initialisation la première fois et conserve la dernière valeur
    qint64 chrono = elapsedTimer.elapsed();
    dt = (chrono - old_chrono) / 1000.0f;
    old_chrono = chrono;

    update();
}
