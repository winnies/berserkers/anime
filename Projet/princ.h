// Basé sur :
// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef PRINC_H
#define PRINC_H

#include "ui_princ.h"
#include "droneanimation.h"

class Princ : public QMainWindow, private Ui::Princ
{
    Q_OBJECT

public:
    explicit Princ(QWidget *parent = nullptr);

private slots:
    void on_play_clicked(bool);
    void on_pause_clicked(bool);
    void on_stop_clicked(bool);
    void on_repeat_toggled(bool);
    void on_slider_sliderMoved(int);
    void on_sliderSpin_valueChanged(double);
    void on_droneAnimation_positionChanged(float);

    void on_actionTrajectoires_triggered(bool checked);
    void on_actionAxe_du_rep_re_triggered(bool checked);
    void on_actionGrille_triggered(bool checked);
    void on_actionNom_des_drones_triggered(bool checked);
    void on_actionLignes_verticales_triggered(bool checked);
    void on_actionOutils_triggered(bool checked);

    void computeCollisions();
    void computeLimits();

    void on_actionAnim_1_triggered();
    void on_actionAnim_2_triggered();

    void on_actionAnim_3_triggered();

private:
    DroneAnimation* droneAnimation;
    bool updateAnimPos = true;
};

#endif // PRINC_H
