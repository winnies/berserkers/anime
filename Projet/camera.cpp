#include "camera.h"
#include <QtMath>

Camera::Camera(const QVector3D& position)
{
    m_position = position;
    updateDir(0, 0);
    m_right = QVector3D::crossProduct(m_direction, QVector3D(0, 1, 0));
}

void Camera::updateDir(int dx, int dy)
{
    float sensitivity = 0.1f;
    dx *= sensitivity;
    dy *= sensitivity;

    m_yaw += dx;
    m_pitch += dy;

    if(m_pitch > 89.0f) m_pitch = 89.0f;
    if(m_pitch < -89.0f) m_pitch = -89.0f;

    m_direction.setX(cos(qDegreesToRadians(m_yaw)) * cos(qDegreesToRadians(m_pitch)));
    m_direction.setY(sin(qDegreesToRadians(m_pitch)));
    m_direction.setZ(sin(qDegreesToRadians(m_yaw)) * cos(qDegreesToRadians(m_pitch)));
    m_direction.normalize();
    m_right = QVector3D::crossProduct(m_direction, QVector3D(0, 1, 0)).normalized();
}

bool Camera::updatePos(int key)
{
    switch(key)
    {
    case Qt::Key_Z :
        m_position += m_direction * m_speed;
        break;
    case Qt::Key_S :
        m_position -= m_direction * m_speed;
        break;
    case Qt::Key_D :
        m_position += m_right * m_speed;
        break;
    case Qt::Key_Q :
        m_position -= m_right * m_speed;
        break;
    case Qt::Key_E :
        m_position += QVector3D(0, 1, 0) * m_speed;
        break;
    case Qt::Key_A :
        m_position -= QVector3D(0, 1, 0) * m_speed;
        break;
    default:
        return false;
    }
    return true;
}

QMatrix4x4 Camera::getViewMatrix() const
{
    QMatrix4x4 viewMatrix;
    viewMatrix.lookAt(m_position, m_position + m_direction, QVector3D(0, 1, 0));
    return viewMatrix;
}
