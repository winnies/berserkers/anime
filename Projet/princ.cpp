// Basé sur :
// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "princ.h"
#include <QDebug>
#include <cmath>

Princ::Princ(QWidget *parent)
    : QMainWindow(parent)
    , droneAnimation(new DroneAnimation(this))
{
    setupUi(this);
    //    toolBox->setVisible(false);
    glarea->droneAnimation = droneAnimation;
    actionOutils->setChecked(true);

    connect(droneAnimation, &DroneAnimation::positionChanged, this, &Princ::on_droneAnimation_positionChanged);
    connect(collisionRadius, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &Princ::computeCollisions);
    connect(limitThreshold, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &Princ::computeLimits);

    repeat->setChecked(true);
    actionNom_des_drones->trigger();

    static QMetaObject::Connection co;
    co = connect(glarea, &GLArea::aboutToCompose, [&](){
        disconnect(co);
        on_actionAnim_1_triggered();
    });
}

void Princ::on_play_clicked(bool)
{
    droneAnimation->play();
}

void Princ::on_pause_clicked(bool)
{
    droneAnimation->pause();
}

void Princ::on_stop_clicked(bool)
{
    droneAnimation->stop();
}

void Princ::on_repeat_toggled(bool checked)
{
    droneAnimation->repeat = checked;
}

void Princ::on_slider_sliderMoved(int)
{
    if (!updateAnimPos) return;
    updateAnimPos = false;
    float pos = float(slider->value()) / float(slider->maximum());
    droneAnimation->setPosition(pos);
    sliderSpin->setValue(pos);
    updateAnimPos = true;
}

void Princ::on_sliderSpin_valueChanged(double pos)
{
    if (!updateAnimPos) return;
    updateAnimPos = false;
    droneAnimation->setPosition(pos);
    slider->setValue(pos * slider->maximum());
    updateAnimPos = true;
}

void Princ::on_droneAnimation_positionChanged(float pos)
{
    if (!updateAnimPos) return;
    updateAnimPos = false;
    slider->setValue(pos * slider->maximum());
    sliderSpin->setValue(pos);
    updateAnimPos = true;
}

void Princ::on_actionTrajectoires_triggered(bool checked)
{
    glarea->showPath = checked;
}

void Princ::on_actionAxe_du_rep_re_triggered(bool checked)
{
    glarea->showAxis = checked;
}

void Princ::on_actionGrille_triggered(bool checked)
{
    glarea->showGrid = checked;
}

void Princ::on_actionNom_des_drones_triggered(bool checked)
{
    glarea->showName = checked;
}

void Princ::on_actionLignes_verticales_triggered(bool checked)
{
    glarea->showVerticalLines = checked;
}

void Princ::on_actionOutils_triggered(bool checked)
{
    toolBox->setVisible(checked);
}

void Princ::computeCollisions()
{
    collisionList->clear();
    auto dronesCount = droneAnimation->dronesCount();
    std::vector<QVector3D> positions;
    positions.resize(dronesCount);
    float collisionRadiusSquared = std::pow(collisionRadius->value(), 2);

    droneAnimation->stop();
    size_t max_t = 100;
    for (size_t t = 0; t <= max_t; ++t) {
        float pos = float(t) / float(max_t);
        droneAnimation->setPosition(pos);

        for (size_t drone = 0; drone < dronesCount; ++drone) {
            positions[drone] = droneAnimation->matrix(drone) * QVector3D(0, 0, 0);
        }
        for (size_t i = 0; i < dronesCount; ++i) {
            for (size_t j = 0; j < dronesCount; ++j) {
                if (i == j || (positions[i] - positions[j]).lengthSquared() > collisionRadiusSquared) continue;
                collisionList->addItem(QString("Drone %1 et %2 à %3").arg(i).arg(j).arg(pos));
            }
        }
    }

    droneAnimation->stop();
}

void Princ::computeLimits()
{
    limitList->clear();
    auto dronesCount = droneAnimation->dronesCount();
    size_t max_t = 100;
    float limitThresholdSquared = std::pow(limitThreshold->value(), 2);

    for (size_t drone = 0; drone < dronesCount; ++drone) {
        for (size_t t = 1; t < max_t; ++t) {
            float t1 = float(t-1) / float(max_t), t2 = float(t) / float(max_t);

            droneAnimation->setPosition(t1);
            auto pos1 = droneAnimation->matrix(drone) * QVector3D(0, 0, 0);

            droneAnimation->setPosition(t2);
            auto pos2 = droneAnimation->matrix(drone) * QVector3D(0, 0, 0);

            if (limitThresholdSquared > (pos1-pos2).lengthSquared()) continue;
            limitList->addItem(QString("Drone %1 entre %3 et %4").arg(drone).arg(t1).arg(t2));
        }
    }

    droneAnimation->stop();
}

void Princ::on_actionAnim_1_triggered()
{
    droneAnimation->loadFile(":/waypoints.json");
    glarea->updateAnim();
}

void Princ::on_actionAnim_2_triggered()
{
    droneAnimation->loadFile(":/super_anim.json");
    glarea->updateAnim();
}

void Princ::on_actionAnim_3_triggered()
{
    droneAnimation->loadFile(":/anim_test_col.json");
    glarea->updateAnim();
}
