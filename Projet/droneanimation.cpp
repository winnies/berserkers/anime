#include "droneanimation.h"
#include <algorithm>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <iostream>
#include <QtMath>

DroneAnimation::DroneAnimation(QObject* parent)
    : QObject(parent)
    , timer(new QTimer(this))
{
    connect(timer, &QTimer::timeout, this, &DroneAnimation::onTimeout);
}

void DroneAnimation::play()
{
    elapsedTimer.start();
    timer->start();
}

void DroneAnimation::pause()
{
    timer->stop();
    elapsedTimer.invalidate();
}

void DroneAnimation::stop()
{
    pause();
    setPosition(0.f);
}

float DroneAnimation::position() const
{
    return current_frame / last_frame;
}

void DroneAnimation::setPosition(float pos)
{
    assert(0.f <= pos && pos <= 1.f);
    float new_frame = pos * last_frame;
    if (new_frame == current_frame) return;
    current_frame = new_frame;
    emit positionChanged(pos);
}

QMatrix4x4 DroneAnimation::matrix(size_t drone) const
{
    assert(drone < drones.size());
    const auto& anim = drones[drone];
    auto second = std::upper_bound(anim.begin(), anim.end(), current_frame,
              [](float value, const Frame& f){
                  return value < f.frame;
              });

    if (second == anim.end()) {
        second = anim.end() - 1;
    }

    auto first = second - 1;
    Frame f1 = *first;
    Frame f2 = *second;
    float f1_frame = f1.frame, f2_frame = f2.frame;
    float adv = (current_frame - f1_frame) / (f2_frame - f1_frame);

    QMatrix4x4 move;
    move.translate(((1.f - adv) * f1.position + adv * f2.position) * 0.01);
    move.rotate(QQuaternion::nlerp(f1.rotation, f2.rotation, adv));

    return move;
}

void DroneAnimation::loadFile(QString filename)
{
    //QFile loadFile(QStringLiteral(":/waypoints.json"));
    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        assert(false);
    }

    current_frame = 0;
    last_frame = 0;

    auto doc = QJsonDocument::fromJson(loadFile.readAll());
    auto root = doc.object();
    framerate = root["framerate"].toDouble();
    timer->setInterval(15);
    auto anims_arr = root["drones"].toArray();
    drones.clear();
    drones.reserve(anims_arr.size());
    for (auto drone : anims_arr) {
        auto d = drone.toObject();
        auto frames = d["waypoints"].toArray();
        std::vector<Frame> fs;
        fs.reserve(frames.size());
        for (auto frame : frames) {
            auto f_obj = frame.toObject();
            Frame f;
            f.frame = f_obj["frame"].toDouble();
            auto f_pos = f_obj["position"].toObject();
            f.position = QVector3D(f_pos["lng_X"].toDouble(),
                                   f_pos["alt_Y"].toDouble(),
                                   f_pos["lat_Z"].toDouble());
            if (f_obj.contains("rotation")) {
                auto f_rot = f_obj["rotation"].toArray();
                f.rotation = QQuaternion::fromEulerAngles(qRadiansToDegrees(f_rot.at(0).toDouble()),
                                                          qRadiansToDegrees(f_rot.at(1).toDouble()),
                                                          qRadiansToDegrees(f_rot.at(2).toDouble()));
            }
            fs.push_back(f);
        }
        std::sort(fs.begin(), fs.end());
        last_frame = std::max(last_frame, fs.back().frame);
        drones.push_back(fs);
    }
}

void DroneAnimation::onTimeout()
{
    float dt = framerate * (elapsedTimer.restart() / 1000.0f);
    current_frame += dt;

    if (current_frame > last_frame) {
        if (repeat) {
            current_frame = dt;
        } else {
            current_frame = last_frame;
            pause();
        }
    }

    emit positionChanged(current_frame / last_frame);
}
