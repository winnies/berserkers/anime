uniform vec3 lightPos;
uniform vec3 lightColor;
uniform sampler2D texture;

varying vec3 fragPos;
varying vec3 normal;
varying vec2 uv;

void main()
{
    // Lumirère ambiente
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;

    // Lumière diffuse
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    // Résultat
    vec3 color = texture2D(texture, uv).rgb;
    vec3 result = (ambient + diffuse) * color;
    gl_FragColor = vec4(result, 1.0);
}
