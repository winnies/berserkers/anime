attribute vec4 in_position;
attribute vec2 in_uv;
varying vec2 uv;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform vec2 particleSize;
uniform float time;
uniform float total_time;

void main() {
    uv = in_uv;

    mat4 modelView = viewMatrix * modelMatrix;

    // First column
    modelView[0][0] = 1.f;
    modelView[0][1] = 0.f;
    modelView[0][2] = 0.f;

    // Second column
    modelView[1][0] = 0.f;
    modelView[1][1] = 1.f;
    modelView[1][2] = 0.f;

    // Thrid column
    modelView[2][0] = 0.f;
    modelView[2][1] = 0.f;
    modelView[2][2] = 1.f;

    //float factor = 1.f + (1.f  - time/total_time);

    //vec4 pos = vec4(vec3(in_position) * particleSize * factor, 1.f);
    //gl_Position = projectionMatrix * modelView * pos;
    gl_Position = projectionMatrix * modelView * (in_position * vec4(particleSize, 1.f, 1.f));
}
