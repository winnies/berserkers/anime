uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

attribute vec3 in_position;
attribute vec3 in_normal;
attribute vec2 in_uv;

varying vec3 fragPos;
varying vec3 normal;
varying vec2 uv;

void main()
{
    fragPos = vec3(modelMatrix * vec4(in_position, 1.0));
    uv = in_uv;
    normal = normalMatrix * in_normal;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(in_position, 1.0f);
}
