varying vec2 uv;
uniform sampler2D texture;
uniform float time;
uniform float total_time;
uniform vec4 color;

void main() {
    float calc = ((total_time - time) / total_time) * 16.f;
    float col = floor(mod(calc, 4.f));
    float row = floor(calc / 4.f);
    //gl_FragColor = texture2D(texture, vec2((uv.s + col)/4.f, (uv.t + row)/4.f)) * vec4(1.f, 1.f, 1.f, time/total_time) * color;
    gl_FragColor = texture2D(texture, uv);
}
