// Basé sur :
// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QElapsedTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLDebugMessage>

#include "camera.h"
#include "droneanimation.h"

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = nullptr);
    ~GLArea() override;
    DroneAnimation* droneAnimation;
    bool showPath = false;
    bool showAxis = false;
    bool showGrid = false;
    bool showName = false;
    bool showVerticalLines = false;

    void updateAnim();

protected slots:
    void onTimeout();

    void messageLogged(QOpenGLDebugMessage message) {
      qDebug() << message;
    }

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

    static QImage renderText(const QString& text);

private:
    float dt = 0;
    float windowRatio = 1.0f;
    float worldSize = 40.f;
    float cellSize = 10.f;
    size_t drone_vert_nb;

    Camera camera;
    QTimer *timer = nullptr;
    QElapsedTimer elapsedTimer;

    QOpenGLBuffer vbo_sol;
    QOpenGLShaderProgram *program_sol;
    QOpenGLTexture *texture_sol;

    QOpenGLBuffer vbo_drone_verts, vbo_drones_elems;
    QOpenGLShaderProgram *program_drone;
    QOpenGLTexture *texture_drone;

    QOpenGLBuffer vbo_axis;
    QOpenGLBuffer vbo_path;
    QOpenGLBuffer vbo_grid;
    QOpenGLBuffer vbo_vert_lines;
    QOpenGLShaderProgram *program_lines;

    QOpenGLBuffer vbo_skybox;
    QOpenGLShaderProgram *program_skybox;
    QOpenGLTexture *texture_skybox;

    QVector<QOpenGLTexture*> textures_names;
    QVector<QVector2D> textures_names_size;

    QOpenGLBuffer vbo_billboard;
    QOpenGLShaderProgram *program_billboard;

    void makeGLObjects();
    void tearGLObjects();
};

#endif // GLAREA_H
