#ifndef DRONEANIMATION_H
#define DRONEANIMATION_H

#include <QVector3D>
#include <QMatrix4x4>
#include <QTimer>
#include <QElapsedTimer>
#include <QQuaternion>

class DroneAnimation : public QObject
{
    Q_OBJECT

public:
    DroneAnimation(QObject* parent = nullptr);

    void loadFile(QString filename);

    void play();
    void pause();
    void stop();

    bool repeat = false;

    float position() const;
    // pos de 0 à 1
    void setPosition(float pos);

    struct Frame {
        float frame;
        QVector3D position;
        QQuaternion rotation;

        bool operator<(const Frame& o) {
            return frame < o.frame;
        }
    };

    std::vector<std::vector<Frame>> drones;
    size_t dronesCount() const { return drones.size(); }
    size_t framesCount() const { return drones[0].size(); }

    QMatrix4x4 matrix(size_t drone) const;

signals:
    void positionChanged(float pos);

private:
    void onTimeout();

    QTimer* timer;
    QElapsedTimer elapsedTimer;
    float current_frame = 0, last_frame;
    float framerate;
};

#endif // DRONEANIMATION_H
