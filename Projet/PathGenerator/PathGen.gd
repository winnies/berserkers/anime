extends Spatial

export(int) var framerate = 30
export(int) var nb_frames = 30
export(float) var cm_scale = 100
export(String) var export_filename = "anim"

func _ready():
	var drones = []
	for child in get_children():
		if not child is Path:
			continue
		
		var pathfollow = child.get_child(0)
		
		var waypoints = []
		pathfollow.unit_offset = 0
		for i in range(nb_frames+1):
			pathfollow.unit_offset = i / float(nb_frames)
			print(i / float(nb_frames))

			var pos = pathfollow.global_transform.origin * cm_scale
			var rot = pathfollow.global_transform.basis.get_euler()
			print(pos)
			waypoints.append({
				"frame": i,
				"position": {
					"lng_X": pos.x,
					"alt_Y": pos.y,
					"lat_Z": pos.z,
				},
				"rotation": [rot.x, rot.y, rot.z]
			})
		
		drones.append({
			"waypoints": waypoints
		})

	var root = {
		"framerate": framerate,
		"drones": drones
	}
	
	var file = File.new()
	file.open(export_filename + ".json", File.WRITE)
	file.store_string(to_json(root))
	file.close()
	
	get_tree().quit(0)

