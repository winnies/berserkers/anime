# Projet d'animation

## Contrôle de la caméra
 - souris pour changer la direction regardée
 - Z, Q, S, D se déplacer par rapport à la direction regardée
 - A, E monter / descendre
 
## PathGenerator

Programme Godot permetant de générer une animation à partir de plusieurs courbes de bézier.
