#include "ground.h"

Ground::Ground()
{}

Ground::Ground(QVector3D position, float taille):
    m_position(position), m_taille(taille)
{
    // Remplissage du vbo avec les sommets et les coordonnées UV
    GLfloat vertices[] = {
       -m_taille, 0.0f,-m_taille,
       -m_taille, 0.0f, m_taille,
        m_taille, 0.0f, m_taille,
        m_taille, 0.0f, m_taille,
        m_taille, 0.0f,-m_taille,
       -m_taille, 0.0f,-m_taille
    };

    GLfloat texCoords[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f
        };

    QVector<GLfloat> vertData;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData.append(vertices[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData.append(texCoords[i*2+j]);
    }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
}

void Ground::display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix)
{
    m_vbo.bind();
    m_shader_program->bind(); // active le shader program du sol

    QMatrix4x4 modelMatrixSol;
    modelMatrixSol.translate(m_position);
    m_shader_program->setUniformValue("projectionMatrix", projectionMatrix);
    m_shader_program->setUniformValue("viewMatrix", viewMatrix);
    m_shader_program->setUniformValue("modelMatrix", modelMatrixSol);

    m_shader_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    m_shader_program->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    m_shader_program->enableAttributeArray("in_position");
    m_shader_program->enableAttributeArray("in_uv");

    m_texture->bind();
    glDrawArrays(GL_TRIANGLES, 0, 6);
    m_texture->release();

    m_shader_program->disableAttributeArray("in_position");
    m_shader_program->disableAttributeArray("in_uv");
    m_shader_program->release();
}

void Ground::attachTexture(QOpenGLTexture* texture)
{
    m_texture = texture;
}

void Ground::attachShaderProgram(QOpenGLShaderProgram* shaderProgram)
{
    m_shader_program = shaderProgram;
}

void Ground::destroy()
{
    m_vbo.destroy();
    delete m_texture;
}
