#ifndef SMOKE_H
#define SMOKE_H

#include <stdlib.h>
#include <time.h>
#include "puff.h"

class Smoke
{
public:
    Smoke();
    Smoke(QVector3D position, float timeInterval, QColor color);

public:
    void animate(float dt);
    void display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix);
    void attachTexture(QOpenGLTexture* texture);
    void attachShaderProgram(QOpenGLShaderProgram* shaderProgram);
    void destroy();

private:
    QVector3D m_position;
    std::list<Puff> m_puffsList;
    float m_timeInterval;
    float m_elapsedTime;
    QColor m_color;

    QOpenGLShaderProgram *m_shader_program;
    QOpenGLTexture *m_texture;
};

#endif // SMOKE_H
