#ifndef GROUND_H
#define GROUND_H


#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>

class Ground
{
public:
    Ground();
    Ground(QVector3D position, float taille);

public:
    void display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix);
    void attachTexture(QOpenGLTexture* texture);
    void attachShaderProgram(QOpenGLShaderProgram* shaderProgram);
    void destroy();

private:
    QVector3D m_position;
    float m_taille;

    QOpenGLShaderProgram *m_shader_program;
    QOpenGLBuffer m_vbo;
    QOpenGLTexture *m_texture;
};
#endif // GROUND_H
