#include "puff.h"
#include <iostream>

Puff::Puff()
{}

Puff::Puff(QVector3D position, float taille, QVector3D vitesse, float duree, QColor color):
    m_position(position), m_taille(taille), m_taille_init(taille), m_vitesse(vitesse),
    m_duree(duree), m_duree_init(duree), m_color(color)
{
    // Remplissage du vbo avec les sommets et les coordonnées UV
    GLfloat vertices[] = {
       -taille, taille, 0.0f,
       -taille, -taille, 0.0f,
        taille, taille, 0.0f,
       -taille,-taille, 0.0f,
        taille,-taille, 0.0f,
        taille, taille, 0.0f
    };

    GLfloat texCoords[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
        };

    QVector<GLfloat> vertData;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData.append(vertices[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData.append(texCoords[i*2+j]);
    }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
}

void Puff::animate(float dt)
{
    m_position += m_vitesse * dt;
    m_duree -= dt;
}

void Puff::display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix)
{
    // Affichage d'une particule
    m_vbo.bind();
    m_shader_program->bind(); // active le shader program des particules

    QMatrix4x4 modelMatrix;
    modelMatrix.translate(m_position);

    m_shader_program->setUniformValue("projectionMatrix", projectionMatrix);
    m_shader_program->setUniformValue("viewMatrix", viewMatrix);
    m_shader_program->setUniformValue("modelMatrix", modelMatrix);
    m_shader_program->setUniformValue("particleSize", m_taille);
    m_shader_program->setUniformValue("total_time", m_duree_init);
    m_shader_program->setUniformValue("time", m_duree);
    m_shader_program->setUniformValue("color", m_color);

    m_shader_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    m_shader_program->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    m_shader_program->enableAttributeArray("in_position");
    m_shader_program->enableAttributeArray("in_uv");

    m_texture->bind();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisable(GL_BLEND);
    m_texture->release();

    m_shader_program->disableAttributeArray("in_position");
    m_shader_program->disableAttributeArray("in_uv");
    m_shader_program->release();
}

void Puff::attachTexture(QOpenGLTexture* texture)
{
    m_texture = texture;
}

void Puff::attachShaderProgram(QOpenGLShaderProgram* shaderProgram)
{
    m_shader_program = shaderProgram;
}

float Puff::remainingTime()
{
    return m_duree;
}

void Puff::destroy()
{
    m_vbo.destroy();
}
