varying vec4 uv;
uniform sampler2D texture;
uniform float time;
uniform float total_time;
uniform vec4 color;

void main() {
    float calc = ((total_time - time) / total_time) * 16.0;
    float col = floor(mod(calc, 4.0));
    float row = floor(calc / 4.0);
    gl_FragColor = texture2D(texture, vec2((uv.s + col)/4, (uv.t + row)/4)) * vec4(1.0, 1.0, 1.0, time/total_time) * color;
}
