#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "smoke.h"

Smoke::Smoke()
{}

Smoke::Smoke(QVector3D position, float timeInterval, QColor color):
    m_position(position), m_timeInterval(timeInterval), m_color(color)
{
    m_puffsList = std::list<Puff>();
    m_elapsedTime = 0;
}

void Smoke::animate(float dt)
{
    m_elapsedTime += dt;

    // Ajout nouvelle bouffée
    if(m_elapsedTime >= m_timeInterval)
    {
        m_elapsedTime = 0;
        srand (time(NULL));
        int duree = rand() % 5 + 15;
        Puff puff(m_position, 2.0, QVector3D(0.0f, 1.0f, 0.0f), duree, m_color);
        puff.attachShaderProgram(m_shader_program);
        puff.attachTexture(m_texture);
        m_puffsList.push_front(puff);
    }

    // Suppression des bouffées terminées
    std::list<Puff>::iterator i;
    i = m_puffsList.begin();
    while(i != m_puffsList.end())
    {
        if(i->remainingTime() <= 0)
            i = m_puffsList.erase(i);
        else
            i++;
    }

    // Calcul des nouvelles positions
    for(Puff& puff : m_puffsList)
    {
        puff.animate(dt);
    }
}

void Smoke::display(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix)
{
    for(Puff& puff : m_puffsList)
    {
        puff.display(projectionMatrix, viewMatrix);
    }
}

void Smoke::attachTexture(QOpenGLTexture* texture)
{
    m_texture = texture;
}

void Smoke::attachShaderProgram(QOpenGLShaderProgram* shaderProgram)
{
    m_shader_program = shaderProgram;
}

void Smoke::destroy()
{
    for(Puff& puff: m_puffsList)
    {
        puff.destroy();
    }
    delete m_texture;
    delete m_shader_program;
}
