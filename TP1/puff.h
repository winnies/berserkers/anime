#ifndef PUFF_H
#define PUFF_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>

class Puff
{
public:
    Puff();
    Puff(QVector3D position, float taille, QVector3D vitesse, float duree, QColor color);

public:
    void animate(float dt);
    void display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix);
    void attachTexture(QOpenGLTexture* texture);
    void attachShaderProgram(QOpenGLShaderProgram* shaderProgram);
    float remainingTime();
    void destroy();

private:
    QVector3D m_position;
    float m_taille;
    float m_taille_init;
    QVector3D m_vitesse;
    float m_duree;
    float m_duree_init;
    QColor m_color;

    QOpenGLShaderProgram *m_shader_program;
    QOpenGLBuffer m_vbo;
    QOpenGLTexture *m_texture;
};

#endif // PUFF_H
