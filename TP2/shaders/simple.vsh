attribute vec4 in_position;
attribute vec4 in_uv;
varying vec4 uv;

uniform mat4 u_projection_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_model_matrix;

void main() {
   uv = in_uv;
   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * in_position;
}
