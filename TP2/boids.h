#ifndef BOIDS_H
#define BOIDS_H

#include <QVector3D>

struct Boid {
    struct Neighborhood {
        float distance, angle;
    };

    size_t id, group;
    QVector3D position, velocity;

    float separation_coef, alignment_coef, cohesion_coef;
    Neighborhood separation_n, alignment_n, cohesion_n;
};

class Boids
{
public:
    Boids();

    void update(float dt);

    QVector3D separation(const Boid& b) const;
    QVector3D alignment(const Boid& b) const;
    QVector3D cohesion(const Boid& b) const;

    void foreach_neighbors(const Boid& b, const Boid::Neighborhood& n, std::function<void(const Boid&)> op) const;

    std::vector<Boid> boids;
};

#endif // BOIDS_H
