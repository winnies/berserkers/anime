#ifndef POISSON_H
#define POISSON_H

#include <QVector3D>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

struct Boite
{
    float longueur;
    float largeur;
    float profondeur;
    bool collision(const QVector3D& position) const
    {
        if(abs(position.x()) >= longueur / 2
           || abs(position.y()) >= largeur / 2
           || abs(position.z()) >= profondeur / 2)
        {
            return true;
        }
        return false;
    }
};

class Poisson
{
public:
    Poisson();
    Poisson(const QVector3D& position, const QVector3D& vitesse, float taille, float distance, float manoeuvrabilite);

public:
    void anime(float dt);
    bool dans_voisinage(const QVector3D& p) const;
    void affiche(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix);
    void attach_shader_program(QOpenGLShaderProgram *shader_program);
    void attach_texture(QOpenGLTexture *texture);
    void destroy();

    const QVector3D& position() const;
    const QVector3D& vitesse() const;
    float distance() const;

    void ajuster_vitesse(const Boite& boite, const QVector3D& v);

private:
    QVector3D m_position;
    QVector3D m_vitesse;
    float m_taille;
    float m_distance;
    float m_manoeuvrabilite;

    QOpenGLTexture *m_texture;
    static QOpenGLBuffer m_vbo;
    QOpenGLShaderProgram *m_shader_program;
};

#endif // POISSON_H
