#include "boids.h"
#include <cmath>

const float max_speed = 2.f, max_force = 0.05f;

void limit(QVector3D& v, float mag_max) {
    float mag_squared = v.lengthSquared();
    float mag_max_squered = mag_max * mag_max;
    if (mag_squared > mag_max_squered) {
        (v /= std::sqrt(mag_squared)) *= mag_max;
    }
}

Boids::Boids()
{

}

void Boids::update(float dt)
{
    for (auto& b : boids) {
        QVector3D accel(0.f, 0.f, 0.f);
        accel += separation(b);
        accel += alignment(b);
        accel += cohesion(b);

        b.velocity += accel;
        limit(b.velocity, max_speed);

        b.position += b.velocity * dt;
    }
}

QVector3D Boids::separation(const Boid &b) const
{
    QVector3D c(0.f, 0.f, 0.f);
    float n_neighbors = 0.f;
    foreach_neighbors(b, b.separation_n, [&](const Boid& other) {
        c += (b.position - other.position).normalized() / (b.position - other.position).length();
        n_neighbors += 1.f;
    });

    if (n_neighbors > 0) {
        c /= n_neighbors;
    }

    if (c.length() > 0) {
        c.normalize();
        c *= max_speed;
        c -= b.velocity;
        limit(c, max_force);
    }

    return c;
}

QVector3D Boids::alignment(const Boid &b) const
{
    QVector3D pv(0.f, 0.f, 0.f); // perceived velocity
    float n_neighbors = 0.f;
    foreach_neighbors(b, b.alignment_n, [&](const Boid& other) {
        pv += other.velocity;
        n_neighbors += 1.f;
    });

    if (n_neighbors > 0) {
        pv /= n_neighbors;
        pv.normalize();
        pv *= max_speed;
        pv -= b.velocity;
        limit(pv, max_force);
        pv *= b.alignment_coef;
    }

    return pv;
}

QVector3D Boids::cohesion(const Boid &b) const
{
    QVector3D pc(0.f, 0.f, 0.f); // perceived center of mass
    float n_neighbors = 0.f;
    foreach_neighbors(b, b.cohesion_n, [&](const Boid& other) {
        pc += other.position;
        n_neighbors += 1.f;
    });

    if (n_neighbors > 0) {
        pc /= n_neighbors;
        pc -= b.position;
        pc.normalize();
        pc *= max_speed;
        pc -= b.velocity;
        limit(pc, max_force);
        pc *= b.cohesion_coef;
    }

    return pc;
}

void Boids::foreach_neighbors(const Boid &b, const Boid::Neighborhood &n, std::function<void (const Boid &)> op) const
{
    auto n_distance_squared = n.distance * n.distance;
    auto b_dir_normalized = b.velocity.normalized();
    for (const auto& other_b : boids) {
        if (b.group != other_b.group || b.id == other_b.id) continue;
        auto b_to_other_b = other_b.position - b.position;
        if (b_to_other_b.lengthSquared() > n_distance_squared ||
            QVector3D::dotProduct(b_to_other_b.normalized(), b_dir_normalized) > n.angle) continue;
        op(other_b);
    }
}
