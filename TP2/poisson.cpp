#include "poisson.h"

QOpenGLBuffer Poisson::m_vbo;

Poisson::Poisson()
{}

Poisson::Poisson(const QVector3D& position, const QVector3D& vitesse, float taille, float distance, float manoeuvrabilite):
    m_position(position), m_vitesse(vitesse), m_taille(taille), m_distance(distance), m_manoeuvrabilite(qBound(0.f, manoeuvrabilite, 1.f))
{
    if (!m_vbo.isCreated()) {
        GLfloat vertices[] = {
            0.000000, -0.500000, -0.500000,
            0.000000, 0.500000, 0.000000,
            0.433013, -0.500000, 0.250000,

            0.000000, -0.500000, -0.500000,
            0.433013 ,-0.500000, 0.250000,
            -0.433013 ,-0.500000, 0.250000,

            0.433013, -0.500000, 0.250000,
            0.000000, 0.500000, 0.000000,
            -0.433013, -0.500000, 0.250000,

            -0.433013, -0.500000, 0.250000,
            0.000000, 0.500000, 0.000000,
            0.000000, -0.500000, -0.500000
        };

        GLfloat texCoords[] = {
            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f
        };

        QVector<GLfloat> vertData;
        for (int i = 0; i < 12; ++i) {
            // coordonnées sommets
            for (int j = 0; j < 3; j++)
                vertData.append(vertices[i*3+j] * taille);
            // coordonnées texture
            for (int j = 0; j < 2; j++)
                vertData.append(texCoords[i*2+j]);
        }

        m_vbo.create();
        m_vbo.bind();
        m_vbo.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
    }
}

void Poisson::anime(float dt)
{
    m_position += m_vitesse * dt;
    //qDebug() << m_position;
}

bool Poisson::dans_voisinage(const QVector3D& p) const
{
    return (p - m_position).length() <= m_distance;
}

void Poisson::affiche(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix)
{
    m_vbo.bind();
    m_shader_program->bind();

    QMatrix4x4 modelMatrix;
    modelMatrix.translate(m_position);

    m_shader_program->setUniformValue("u_projection_matrix", projectionMatrix);
    m_shader_program->setUniformValue("u_view_matrix", viewMatrix);
    m_shader_program->setUniformValue("u_model_matrix", modelMatrix);

    m_shader_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    m_shader_program->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    m_shader_program->enableAttributeArray("in_position");
    m_shader_program->enableAttributeArray("in_uv");

    m_texture->bind();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glDisable(GL_BLEND);
    m_texture->release();

    m_shader_program->disableAttributeArray("in_position");
    m_shader_program->disableAttributeArray("in_uv");
    m_shader_program->release();
}

void Poisson::attach_shader_program(QOpenGLShaderProgram *shader_program)
{
    m_shader_program = shader_program;
}

void Poisson::attach_texture(QOpenGLTexture *texture)
{
    m_texture = texture;
}

void Poisson::destroy()
{
    m_vbo.destroy();
}

const QVector3D& Poisson::position() const
{
    return m_position;
}

const QVector3D& Poisson::vitesse() const
{
    return m_vitesse;
}

float Poisson::distance() const
{
    return m_distance;
}

void Poisson::ajuster_vitesse(const Boite& boite, const QVector3D& v)
{
    if(boite.collision(position())) m_vitesse = -m_vitesse;
    if(v == QVector3D(0, 0, 0)) return;
    m_vitesse = (1.f - m_manoeuvrabilite) * m_vitesse + m_manoeuvrabilite * v;
}
