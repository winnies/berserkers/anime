#ifndef BANC_H
#define BANC_H

#include <vector>

#include "poisson.h"
#include "boids.h"

class Banc
{
public:
    Banc();
    Banc(QVector3D position, size_t taille, float largeur, float longueur, float profondeur, float vitesse_max);

public:
    void anime(float dt);
    void affiche(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix);
    void attach_shader_program(QOpenGLShaderProgram *shader_program_aquarium, QOpenGLShaderProgram *shader_program_poisson);
    void attach_texture(QOpenGLTexture *texture);
    void destroy();

private:
    QVector3D m_position;
    size_t m_taille;
    std::vector<Poisson> m_poissons;
    Boite m_boite;
    QOpenGLBuffer m_vbo;
    QOpenGLShaderProgram *m_shader_program;
    float m_vitesse_max;

    Boids m_boids;
    QOpenGLBuffer m_poisson_vbo;
    QOpenGLShaderProgram *m_shader_program_poisson;
    QOpenGLTexture *texture_poisson;
};

#endif // BANC_H
