#include "banc.h"

#include <stdlib.h>
#include <random>

Banc::Banc():
    m_taille(0)
{}

Banc::Banc(QVector3D position, size_t taille, float longueur, float largeur, float profondeur, float vitesse_max):
    m_position(position), m_taille(taille), m_vitesse_max(vitesse_max)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> distrib_longueur(-longueur/8, longueur/8);
    std::uniform_real_distribution<float> distrib_largeur(-largeur/8, largeur/8);
    std::uniform_real_distribution<float> distrib_profondeur(-profondeur/8, profondeur/8);

    std::uniform_real_distribution<float> distrib_dir(-1, 1);
    std::uniform_real_distribution<float> distrib_vitesse(0.1, vitesse_max);

    float taille_poisson = 1.f;

    for(size_t i = 0; i < taille; i++)
    {
        float x = distrib_longueur(gen);
        float y = distrib_largeur(gen);
        float z = distrib_profondeur(gen);

        //QVector3D dir(distrib_dir(gen), distrib_dir(gen), distrib_dir(gen));
        QVector3D dir(0, 0, 1);
        dir.normalize();
        //dir *= distrib_vitesse(gen);

//        m_poissons.push_back(Poisson(QVector3D(x, y, z), dir, 0.5, 3, 0.3));

        Boid b;
        b.id = i;
        b.group = i % 15;
        b.position = {x, y, z};
        b.velocity = dir;

        b.alignment_coef = 1.f;
        b.alignment_n.distance = 45.f/2.5f * taille_poisson;
        b.alignment_n.angle = M_PI * 2.f/3.f;

        b.separation_coef = 1.f;
        b.separation_n.distance = 12.f/2.5f * taille_poisson;
        b.separation_n.angle = M_PI * 2.f/3.f;

        b.cohesion_coef = 0.8f;
        b.cohesion_n.distance = 25.f/2.5f * taille_poisson;
        b.cohesion_n.angle = M_PI * 2.f/3.f;

        m_boids.boids.push_back(b);
    }

    m_boite = Boite{longueur, largeur, profondeur};

    QVector3D v1 = QVector3D(-longueur/2, -largeur/2, profondeur/2);
    QVector3D v2 = QVector3D(-longueur/2, -largeur/2, -profondeur/2);
    QVector3D v3 = QVector3D(longueur/2, -largeur/2, -profondeur/2);
    QVector3D v4 = QVector3D(longueur/2, -largeur/2, profondeur/2);
    QVector3D v5 = QVector3D(-longueur/2, largeur/2, profondeur/2);
    QVector3D v6 = QVector3D(-longueur/2, largeur/2, -profondeur/2);
    QVector3D v7 = QVector3D(longueur/2, largeur/2, -profondeur/2);
    QVector3D v8 = QVector3D(longueur/2, largeur/2, profondeur/2);

    GLfloat vertices[] =
    {
        v1.x(), v1.y(), v1.z(),
        v2.x(), v2.y(), v2.z(),
        v3.x(), v3.y(), v3.z(),
        v4.x(), v4.y(), v4.z(),

        v4.x(), v4.y(), v4.z(),
        v3.x(), v3.y(), v3.z(),
        v7.x(), v7.y(), v7.z(),
        v8.x(), v8.y(), v8.z(),

        v8.x(), v8.y(), v8.z(),
        v7.x(), v7.y(), v7.z(),
        v6.x(), v6.y(), v6.z(),
        v5.x(), v5.y(), v5.z(),

        v5.x(), v5.y(), v5.z(),
        v6.x(), v6.y(), v6.z(),
        v2.x(), v2.y(), v2.z(),
        v1.x(), v1.y(), v1.z(),

        v5.x(), v5.y(), v5.z(),
        v1.x(), v1.y(), v1.z(),
        v4.x(), v4.y(), v4.z(),
        v8.x(), v8.y(), v8.z(),


        v2.x(), v2.y(), v2.z(),
        v6.x(), v6.y(), v6.z(),
        v7.x(), v7.y(), v7.z(),
        v3.x(), v3.y(), v3.z()
    };

    QVector<GLfloat> vertData;
    for (int i = 0; i < 24; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData.append(vertices[i*3+j]);
    }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));


    {
        GLfloat vertices[] = {
            0.000000, -0.500000, -0.500000,
            0.000000, 0.500000, 0.000000,
            0.433013, -0.500000, 0.250000,

            0.000000, -0.500000, -0.500000,
            0.433013 ,-0.500000, 0.250000,
            -0.433013 ,-0.500000, 0.250000,

            0.433013, -0.500000, 0.250000,
            0.000000, 0.500000, 0.000000,
            -0.433013, -0.500000, 0.250000,

            -0.433013, -0.500000, 0.250000,
            0.000000, 0.500000, 0.000000,
            0.000000, -0.500000, -0.500000
        };

        GLfloat texCoords[] = {
            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f,

            0.0f, 1.0f,
            0.5f, 0.0f,
            1.0f, 1.0f
        };

        QVector<GLfloat> vertData;
        for (int i = 0; i < 12; ++i) {
            // coordonnées sommets
            for (int j = 0; j < 3; j++)
                vertData.append(vertices[i*3+j] * taille_poisson);
            // coordonnées texture
            for (int j = 0; j < 2; j++)
                vertData.append(texCoords[i*2+j]);
        }

        m_poisson_vbo.create();
        m_poisson_vbo.bind();
        m_poisson_vbo.allocate(vertData.constData(), vertData.count() * int(sizeof(GLfloat)));
    }
}

void Banc::anime(float dt)
{
    m_boids.update(dt);
    for (auto& b : m_boids.boids) {
        if (!m_boite.collision(b.position)) continue;
        b.velocity = -b.velocity;
        b.position += b.velocity * dt;
    }

//    auto position_moyenne = [&](const std::vector<Poisson>& poissons)
//    {
//        QVector3D acc(0, 0, 0);
//        for(const Poisson& p : poissons)
//        {
//            acc += p.position();
//        }
//        return acc/poissons.size();
//    };

//    auto vitesse_moyenne = [&](const std::vector<Poisson>& poissons)
//    {
//        QVector3D acc(0, 0, 0);
//        for(const Poisson& p : poissons)
//        {
//            acc += p.vitesse();
//        }
//        return acc/poissons.size();
//    };

//    auto chercher_voisins = [&](size_t index)
//    {
//        std::vector<Poisson> voisins;
//        for(size_t i = 0; i < m_poissons.size(); i++)
//        {
//            if(i == index) continue;
//            if(m_poissons[i].dans_voisinage(m_poissons[index].position()))
//                voisins.push_back(m_poissons[i]);
//        }
//        return voisins;
//    };

//    auto separation = [&](size_t index, std::vector<Poisson>& voisins)
//    {
//        return (m_poissons[index].position() - position_moyenne(voisins)).normalized();
//    };

//    auto alignement = [&](size_t index, std::vector<Poisson>& voisins)
//    {
//        return m_poissons[index].vitesse() - vitesse_moyenne(voisins);
//    };

//    auto cohesion = [&](size_t index, std::vector<Poisson>& voisins)
//    {
//        return position_moyenne(voisins) - m_poissons[index].position();
//    };

//    std::vector<QVector3D> calc_adjs;
//    calc_adjs.reserve(m_poissons.size());

//    for(size_t i = 0; i < m_poissons.size(); i++)
//    {
//        QVector3D v;

//        std::vector<Poisson> voisins = chercher_voisins(i);
//        if (!voisins.empty()) {
//            v = 0.5 * separation(i, voisins) + 1.0 * alignement(i, voisins) + 0.5 * cohesion(i, voisins);
//            if(v.length() > m_vitesse_max) {
//                v.normalize();
//                v = v * m_vitesse_max;
//            }
//        }
//        calc_adjs.push_back(v);
//    }

//    for(size_t i = 0; i < m_poissons.size(); i++) {
//        m_poissons[i].ajuster_vitesse(m_boite, calc_adjs[i]);
//        m_poissons[i].anime(dt);
//    }
}

void Banc::affiche(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix)
{
    m_vbo.bind();
    m_shader_program->bind();

    QMatrix4x4 modelMatrix;
    modelMatrix.translate(m_position);

    m_shader_program->setUniformValue("u_projection_matrix", projectionMatrix);
    m_shader_program->setUniformValue("u_view_matrix", viewMatrix);
    m_shader_program->setUniformValue("u_model_matrix", modelMatrix);

    m_shader_program->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 3 * sizeof(GLfloat));
    m_shader_program->enableAttributeArray("in_position");

    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDrawArrays(GL_QUADS, 0, 24);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_BLEND);

    m_shader_program->disableAttributeArray("in_position");
    m_shader_program->release();

//    for(Poisson& p : m_poissons)
//        p.affiche(projectionMatrix, viewMatrix);
    {
    m_poisson_vbo.bind();
    m_shader_program_poisson->bind();

    m_shader_program_poisson->setUniformValue("u_projection_matrix", projectionMatrix);
    m_shader_program_poisson->setUniformValue("u_view_matrix", viewMatrix);

    m_shader_program_poisson->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    m_shader_program_poisson->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    m_shader_program_poisson->enableAttributeArray("in_position");
    m_shader_program_poisson->enableAttributeArray("in_uv");

    texture_poisson->bind();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    for (auto& b : m_boids.boids) {
        QVector3D up(0, 1, 0);
        float angle = std::acos(QVector3D::dotProduct(b.velocity.normalized(), up)) / M_PI * 180.f;
        QVector3D pivot = QVector3D::crossProduct(b.velocity, up);

        QMatrix4x4 modelMatrix;
        modelMatrix.translate(b.position);
        modelMatrix.rotate(-angle, pivot);
        m_shader_program_poisson->setUniformValue("u_model_matrix", modelMatrix);

        glDrawArrays(GL_TRIANGLES, 0, 12);
    }

    glDisable(GL_BLEND);
    texture_poisson->release();

    m_shader_program_poisson->disableAttributeArray("in_position");
    m_shader_program_poisson->disableAttributeArray("in_uv");
    m_shader_program_poisson->release();
    }
}

void Banc::attach_shader_program(QOpenGLShaderProgram *shader_program_aquarium, QOpenGLShaderProgram *shader_program_poisson)
{
    m_shader_program = shader_program_aquarium;
//    for(Poisson& p : m_poissons)
//    {
//        p.attach_shader_program(shader_program_poisson);
//    }
    m_shader_program_poisson = shader_program_poisson;
}

void Banc::attach_texture(QOpenGLTexture *texture)
{
//    for(Poisson& p : m_poissons)
//        p.attach_texture(texture);
    texture_poisson = texture;
}

void Banc::destroy()
{
//    for(Poisson& p : m_poissons)
//        p.destroy();
}
